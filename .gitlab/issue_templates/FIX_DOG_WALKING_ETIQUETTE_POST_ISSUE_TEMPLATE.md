## Description

The Good Dogs Project recently published a blog post about dog walking etiquette. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

There are three tasks you must complete. You'll complete your work in the `/content/blog/dog-walking-etiquette.md` file.

### Fix a broken link

- [ ] Fix the broken link in the first paragraph.

### Change headings that use -ing 

- [ ] Change the headings that use -ing form to simple verb form.

### Change a level one heading to a level two heading

- [ ] Change the **Sharing the road and sidewalk** heading from a level one heading to a level two heading.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.